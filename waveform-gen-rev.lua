print("LabJack Lua Waveform Generator rev JT. Version 0.1")
-- This script will output a waveform on DAC0
-- User memory is used to set the scan frequency,
-- wave frequency, wave shape, amplitude and offset.
-- Once the script is running switch to the "Register Matrix" tab and add
-- registers 46000, 46002, 46004, 46006, and 46008. See below for which register
-- affects which attribute. Updating those values will update the output wave.

local mbWrite=MB.W
--MB.W(Address, dataType, value)
--Description: Modbus write. Writes a single value to a modbus register. The type can be a u16, u32, a float, or a string.

--dataTypes

--0:    unsigned 16-bit integer
--1:    unsigned 32-bit integer
--2:    signed 32-bit integer
--3:    single precision floating point (float)
--98:   string
--99:   byte - The "byte" dataType is used to pass arrays of bytes, or tables.

local mbRead=MB.R
--Value = MB.R(Address, dataType)  
--Description: Modbus read. Will read a single value from a modbus register. That item can be a u16, u32, a float 

local OStreamBuffSize = 4096      -- number of bytes to allocate to the buffer
-- OStreamBuffSize should be more than 2 * steamScanFreq / waveFreq else an error message will be provided

local streamScanFreq = 1000      -- How often the DACs value will be updated
local waveFreq = 10              -- Frequency of the sine wave.
local waveAmp = 1.0               -- Wave amplitude
local waveOff = 2.0               -- Wave offset
local refreshInterval = 100

local last_SSF = 0
local last_WF = 0
local last_WS = 0
local last_WA = 0
local last_WO = 0

local static_v = 2
local ain_v = 0
local freqBias = 0.0

mbWrite(4990, 1, 0) -- Stream (0: disable, 1: enable)
mbWrite(4090, 1, 0)                  -- Stream out (0: disable, 1: enable)
mbWrite(4040, 1, 1000)               -- Stream-Out target to 1000 (address of the DAC0)
mbWrite(4050, 1, OStreamBuffSize)    -- Buffer size
mbWrite(4090, 1, 1)                  -- Stream out (0: disable, 1: enable)


mbWrite(1002, 3, static_v) -- set static DAC1 voltage

--LJ.IntervalConfig & LJ.CheckInterval------------------------------------------

--IntervalConfig and CheckInterval work together to make an easy-to-use timing function.
--Set the desired interval time with IntervalConfig, then use CheckInterval to watch for timeouts. 
--The interval period will have some jitter but no overall error. Jitter is typically ±30 µs but can be greater depending on 
--processor loading. A small amount of error is induced when the processors core speed is changed.
--Up to 8 different intervals can be active at a time.

LJ.IntervalConfig(0, refreshInterval)        -- Check for new values once per second
LJ.IntervalConfig(1, 50)          -- Used to limit debugging output rates
--LJ.IntervalConfig(handle, time_ms)

--handle: 0-7 Specifies which of the 8 available timers to configure
--time_ms: Number of milliseconds per interval.
--Description: Set an interval for code execution. This function is included in almost all scripts

--LJ.CheckInterval(handle)

--handle: 0-7 Specifies which of the 8 available timers to check
--Returns: 1 if the interval has expired. 0 if not.
--Description: Check if the timer interval has expired. This function is included in almost all scripts

local checkInterval=LJ.CheckInterval

print("Start generating sine wave")

-- stream output
-- after setting #4990 (stream_enable) to 1, #4400 (stream_buffer) will be executed with a loop size of #4060 (loop_num)
while true do
  if checkInterval(0) then

    -- check the voltage of AIN1 and update waveFreq and streamScanFreq
    ain_v = mbRead(2,3)
    waveFreq = 0.2* 10 ^ ain_v
      
    -- Compute new wave
    -- Note that a quite large nPoints due to large refreshInterval and streamScanFreq cause a buffer error
    local nPoints = refreshInterval / 1000 * streamScanFreq 
    local dt = waveFreq / streamScanFreq
    local dataSet = {}
    local i = 1
    
    for i=1, nPoints, 1 do
      -- sin (2 pi (ft + fb))
      dataSet[i] = waveAmp * math.sin(2 * 3.141592 * (waveFreq * dt * i + freqBias)) + waveOff
    end
    freqBias = freqBias + waveFreq * dt * nPoints
    freqBias = freqBias - math.floor(freqBias)

     -- Load the new wave form
    for i=1, nPoints, 1 do            -- load all the data... one at a time
      mbWrite(4400, 3, dataSet[i])       -- This could imporved to load more quickly
    end                               -- but the benefit will be smaller than when using C-R.
    
    mbWrite(4060, 1, nPoints)            -- Set the number of points to loop
    mbWrite(4070, 1, 1)                  -- Begin using the recently loaded data set.
    

    if mbRead(4990, 1, 0) == 0 then       -- If stream is off because this is the first run or changing scanFreq
      mbWrite(4002, 3, streamScanFreq)     -- Set scan rate
      mbWrite(4004, 1, 1)                  -- 1 Chn per scan
      mbWrite(4008, 3, 0)                  -- Automatic Settling
      mbWrite(4010, 1, 0)                  -- Automatic Resolution
      mbWrite(4012, 1, 256)                -- Use a real small buffer, because we do not care about any data.
      mbWrite(4014, 1, 0)                  -- No advanced clocking options
      mbWrite(4016, 1, 0)                  -- No advanced targets
      mbWrite(4020, 1, 0)                  -- Continuous operation; disable burst.
      mbWrite(4100, 1, 4800)               -- Add channel 4800 (StreamOut 0)
      mbWrite(4990, 1, 1)                  -- Start stream
    end
    
    last_SSF = streamScanFreq
  end
end
# Labjackの制御

## Overview
 - Labjackは，高精度でAD/DAを行う制御機器．
 - サンプリング等の際には，通常モードとStreamモードを利用可能．
   - サンプリングレート数ms程度であれば，通常モードで十分．
   - サンプリングレート数万Hzになると，Streamモードの利用が不可欠．制御命令をまとめて与えてLabjackを自律的に動かすモードで，データを一度Labjackのバッファーに保存する．高機能なぶん，制御は難解．
   - SIGVIEWでLabjackを制御する際には，自動的にStreamモードになる．
   - Stream Outというモードもある．バッファにループを仕込んで，波形を出力する際に使う．（その他にも使い道があるのかもしれないが，わからない）
   - Streamモードを使うなら，`Ethernet`接続が不可欠．

## 購入
 - 代理店を挟むと面倒．直接購入可能．（https://labjack.com/）

## ソフトウェア
 - Labjack設定用に`Kipling`が用意されている．
 - Labjack通常モードおよびStreamモードのモニタリング用に，`LJLogM`および`LJStreamM`が用意されている．
 - 設定ファイルが必要．`LJconfig`フォルダに格納した．
 - SIGVIEWでもStreamモードでモニタリングできる．ver5.0からJuliaスクリプトが利用可能なので，多少細かい制御も可能．

## PC-Labjack接続
 - 通常モードならUSB接続でも良いが，Streamモードを使うなら，Ethernet接続が不可欠．
 - 普通は固定IPが必要なので（DHCPでもできる？），学内ネットワークに接続するのはかなり面倒．USB/LANの変換ケーブルを使ってPC側はUSBで受けるのが良い．
   - Labjack側は，Kiplingから適当なIPアドレスを指定する（たとえば`192.168.1.7`）．
   - PC側は2つのネットワークに接続することになるので，設定が必要．
   - Labjackと同じネットワークに属する固定IPにする（たとえば`192.168.1.1`）．サブネットマスクは`255.255.255.0`，デフォルトゲートウェイは空欄．
   - ルーティングする（しなくてもそのまま動くが）．たとえば，`route add 192.168.1.0 mask 255.255.255.0 192.168.1.7`とする．

## プログラミング
 - Labjackは，Luascriptの他に，python/C++等で制御できる．
 - Luascriptは，実行スピードが早いらしい．また，Labjackのメモリに書き込んで起動時に継続作動が可能．しかし，ファイル入出力が弱い（MicroSDを使うという手はある．
 - 総合的に考えると，pythonが最有力候補．サンプルプログラムを`Python_LJM_2019_04_03`に格納した．
 - pythonプログラミングには，`Anaconda`および`Spyder`を使う．多少のpythonプログラミングであれば，`miniconda`と`Rstudio`の組み合わせも使えなくはないが，動作が不安定で，ggplotに直さないとプロットもできない．
 - Rstudioでやった結果は`stream-basic.Rmd`や`stream-out.Rmd`．
 - Luascriptサンプルは，たとえば，`waveform-gen-rev.lua`参照．ただし最後までチェックできていない．

## AINを使った周波数可変wave generator
 - LabJackのStreamを使えば，相当高周波数の波形まで出力できるが，信頼性が不明で，制御がかなりややこしい．
 - 波形出力はPCのオーディオデバイスに任せ，それの制御をLabjackで行うシステムとする．
 - `pyaudio-stream-sinewave-changefreq-discrete.py`参照．

### Labjack側制御
Labjack側には可変抵抗を接続する．両端に定電圧をかけて，AINで抵抗値（電圧）を読み取る．

```{python:関係するスクリプト}
ljm_handle = ljm.openS("T7","ANY","ANY")
ljm.eWriteName(ljm_handle, ljm_dac_name, ljm_dac_val)

ljm_ain_val   = ljm.eReadName(ljm_handle, ljm_ain_name)

ljm.close(ljm_handle)
```

### Python側制御
PyAudioを使う．PyAudioのインストールは`pip install pipwin`としてから，`pipwin install pyaudio`とする．そうしないと，windowsのオーディオデバイスを上手く認識できない．

`pyaudio.open`でstreamインスタンスを設定する．streamインスタンスには，既にあるファイルを扱うブロッキング処理もあるが，逐次入出力処理を行う非ブロッキング処理を採用する．

```{python:pyaudio.open}
stream = p.open(
    format = pyaudio.paFloat32,
    channels = 1,
    rate = fs,
    frames_per_buffer = buf,
    stream_callback = callback,
    output = True,
    output_device_index = output_dev_id)
```

ポイントは`frames_per_buffer`と`stream_callback`の設定．前者はバッファの設定で，stream時にはバッファを入出力（ここでは出力だけ）する．バッファがなくなる際に次のバッファを設定する処理が`stream_callback`で，今回は周波数可変のサイン波としている．

```{python:callback関数}
#Pyaudio callback function for the streaming configuration
def callback(in_data, frame_count, time_info, status):
    global f_ref
    global offset
    global el_time
    #Read AIN from the Labjack device
    #ljm_ain_val: AIN value
    #ljm_ain_val_s: AIN value scaled to [0,1]
    ljm_ain_val   = ljm.eReadName(ljm_handle, ljm_ain_name)
    if ljm_ain_val < ljm_ain_lim[0]:
        ljm_ain_val_s = 0
    elif ljm_ain_val > ljm_ain_lim[1]:
        ljm_ain_val_s = 1
    else:
        ljm_ain_val_s = (ljm_ain_val - ljm_ain_lim[0])/ (ljm_ain_lim[1] - ljm_ain_lim[0])
    ljm_ain_val_s = round(20.0 * ljm_ain_val_s) / 20.0 #discretize
    #Set wave frequency
    f = f_ref * 10 ** ljm_ain_val_s
    print("\r"+"frequency: %.3f Hz" %(f),end = "")
    #Generate sine wave as bytes array
    y = np.zeros(frame_count, dtype = np.float32)
    y += np.sin(2*np.pi*(np.arange(frame_count)*f/fs+offset))
    offset += buf*f/fs
    offset -= math.floor(offset)
    waveData = y.tobytes()
    el_time += frame_count/fs
    return (waveData, pyaudio.paContinue)
```
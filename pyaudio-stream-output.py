# Generate sine wave
# Discrete frequencies are employed

# load packages
import numpy as np
import pyaudio
import time
import math
import pandas as pd

# read csv

df_freq_char = pd.read_csv("freq-characteristics.csv", index_col = 2)

if df_freq_char.max()["level"] > 0:
    print("Input magnitude is > 0 dB. Stop continuing the program")

def amp_freq(df_freq_char, n):
    level = df_freq_char.at[n,"level"]
    amp = 10 ** (level / 20)
    return(amp)

# set global variables
f_ref = 20 #lower limit of the frequency. the upper limit is 10 * f_ref
fs = 48000 #sampling rate
buf = 1024 #streaming buffer
offset = 0.0 #offset of the wave number to generate the sine wave
duration = 300.0 #max duration of the wave generation
el_time = 0.0 #elapsed time
n = 8
f = 20 * 2 **(n/12) 



#Pyaudio callback function for the streaming configuration
def callback(in_data, frame_count, time_info, status):
    global offset
    global el_time
    global n
    global df_freq_char
    global amp_freq
    amp = amp_freq(df_freq_char, n)
    #print("\r"+"frequency: %.3f Hz" %(f),end = "") 
    #Generate sine wave as bytes array
    y = np.zeros(frame_count, dtype = np.float32)
    y += np.sin(2*np.pi*(np.arange(frame_count)*f/fs+offset)) * amp
    print("\r"+"frequency: %.3f " %(f),end = "") 
    offset += buf*f/fs
    offset -= math.floor(offset)
    waveData = y.tobytes()
    el_time += frame_count/fs
    return (waveData, pyaudio.paContinue)


#Make pyaudio streaming instance
print("Initializing Pyaudio...")
p = pyaudio.PyAudio()


print("Selecting the output device...")
# select output device id   

# First, select the host api. ASIO driver should be selected, but not the conservative MME driver.
# here all the host apis are listed
print("Host APIs are:")
for host_index in range(0, p.get_host_api_count()):
    info_api = p.get_host_api_info_by_index(host_index)
    if info_api["name"] == "ASIO":
        info_ASIO = info_api
        host_index_ASIO = info_ASIO["index"]
    print(info_api)

# Next, select the device linked with the ASIO driver.
# For the present case, TEAC ASIO USB DRIVER is selected
print("Devices linked with the ASIO driver are:")
for device_index in range(0, info_ASIO["deviceCount"]):
    info_device = p.get_device_info_by_host_api_device_index(info_ASIO["index"],device_index)
    if info_device["name"] == "TEAC ASIO USB DRIVER":
        info_TEAC = info_device
        device_index_TEAC = info_TEAC["index"]
    print(info_device)

print("Selected output device is:")
print(info_TEAC)
output_dev_id = info_TEAC["index"]

#Configure and start streaming
stream = p.open(
    format = pyaudio.paFloat32,
    channels = 1,
    rate = fs,
    frames_per_buffer = buf,
    stream_callback = callback,
    output = True,
    output_device_index = output_dev_id)

stream.start_stream()

print("Generating Sine wave...")
print("Wave generation can be stopped by pressing [Ctrl + C]")
while el_time < duration:
    try:
        time.sleep(0.1)
    except KeyboardInterrupt:
        break

#End and close streaming
stream.close()
p.terminate()

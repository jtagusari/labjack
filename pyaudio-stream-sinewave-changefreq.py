# Generate sine wave
# Discrete frequencies are employed

# load packages
import numpy as np
import pyaudio
import time
import math
import pandas as pd
from labjack import ljm
from scipy import interpolate

# load frequency characteristics
df_freq_char = pd.read_csv("freq-characteristics.csv")
freq_char = interpolate.interp1d(df_freq_char["n"], df_freq_char["level"])


# read csv
df_freq_char = pd.read_csv("freq-characteristics.csv", index_col = 2)

if df_freq_char.max()["level"] > 0:
    print("Input magnitude is > 0 dB. Stop continuing the program")

def amp_freq(df_freq_char, n):
    level = df_freq_char.at[n,"level"]
    amp = 10 ** (level / 20)
    return(amp)

# set global variables
f_lower = 20 #lower limit of the frequency. the upper limit is 10 * f_lower
f_upper = 100
fs = 48000 #sampling rate
buf = 1024 #streaming buffer
offset = 0.0 #offset of the wave number to generate the sine wave
duration = 300.0 #max duration of the wave generation
el_time = 0.0 #elapsed time
ljm_dac_name = "DAC0" #Labjack DAC output channel
ljm_dac_val = 5.0 #Labjack DAC output voltage
ljm_ain_lim = [0.5, 4.0] #Labjack AIN lower and upper limits
ljm_ain_name = "AIN0" #Labjack AIN channel


#Labjack configuration
print("Configuring LabJack... (it may take several seconds)", end = "\n\n")
ljm_handle = ljm.openS("T7","ANY","ANY")
ljm.eWriteName(ljm_handle, ljm_dac_name, ljm_dac_val)

#Pyaudio callback function for the streaming configuration
def callback(in_data, frame_count, time_info, status):
    global f_lower
    global f_upper
    global offset
    global el_time
    #Read AIN from the Labjack device
    #ljm_ain_val: AIN value
    #ljm_ain_val_s: AIN value scaled to [0,1]
    ljm_ain_val   = ljm.eReadName(ljm_handle, ljm_ain_name)
    if ljm_ain_val < ljm_ain_lim[0]:
        ljm_ain_val_s = 0
    elif ljm_ain_val > ljm_ain_lim[1]:
        ljm_ain_val_s = 1
    else:
        ljm_ain_val_s = (ljm_ain_val - ljm_ain_lim[0])/ (ljm_ain_lim[1] - ljm_ain_lim[0])
    #Set wave frequency
    f = f_lower * (f_upper/f_lower) ** ljm_ain_val_s
    n = 12 * math.log2(f / 20)
    amp_dB = freq_char(n)
    amp = 10 ** (amp_dB / 20)
    print("\r"+"frequency: %6.1f Hz; amplitude: %5.1f dB" %(f, amp_dB),end = "")
    #Generate sine wave as bytes array
    y = np.zeros(frame_count, dtype = np.float32)
    y += np.sin(2*np.pi*(np.arange(frame_count)*f/fs+offset)) * amp
    offset += buf*f/fs
    offset -= math.floor(offset)
    waveData = y.tobytes()
    el_time += frame_count/fs
    return (waveData, pyaudio.paContinue)


#Make pyaudio streaming instance
print("Initializing Pyaudio...", end = "\n\n")
p = pyaudio.PyAudio()


print("Selecting the output device...")
# select output device id   

# First, select the host api. ASIO driver should be selected, but not the conservative MME driver.
# here all the host apis are listed
print("Host APIs are:")
for host_index in range(0, p.get_host_api_count()):
    info_api = p.get_host_api_info_by_index(host_index)
    if info_api["name"] == "ASIO":
        info_ASIO = info_api
        host_index_ASIO = info_ASIO["index"]
    print(info_api)

# Next, select the device linked with the ASIO driver.
# For the present case, TEAC ASIO USB DRIVER is selected
print("Devices linked with the ASIO driver are:")
for device_index in range(0, info_ASIO["deviceCount"]):
    info_device = p.get_device_info_by_host_api_device_index(info_ASIO["index"],device_index)
    if info_device["name"] == "TEAC ASIO USB DRIVER":
        info_TEAC = info_device
        device_index_TEAC = info_TEAC["index"]
    print(info_device)

print("Selected output device is:")
print(info_TEAC)
output_dev_id = info_TEAC["index"]

#Execute if you use the default device
#output_dev_id = p.get_default_output_device_info()["index"]

print()

#Configure and start streaming
stream = p.open(
    format = pyaudio.paFloat32,
    channels = 1,
    rate = fs,
    frames_per_buffer = buf,
    stream_callback = callback,
    output = True,
    output_device_index = output_dev_id)

stream.start_stream()

print("Generating Sine wave...")
print("Wave generation can be stopped by pressing [Ctrl + C]")
while el_time < duration:
    try:
        time.sleep(0.1)
    except KeyboardInterrupt:
        break

#End and close streaming
stream.close()
p.terminate()
ljm.close(ljm_handle)

